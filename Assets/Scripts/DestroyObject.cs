using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DestroyObject: MonoBehaviour {
    [SerializeField]
    public GameObject toDestroy;
    [SerializeField]
    public int lifeTime = 10;

    public void Start(){
        StartCoroutine(Disappear());
    }

    IEnumerator Disappear(){
        yield return new WaitForSeconds(lifeTime);
        Destroyed();      
    }

    public void Destroyed() {
        Destroy(toDestroy);
    }
}