﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public static PlayerMovement pm;
    public float cameraHeight = 1.6f;

    [SerializeField]
    private Transform player;
    private Transform cam;
    private Transform forwardObject;

    [Header("Movement")]
    [SerializeField]
    private bool isMoving;
    [SerializeField]
    private float moveSpeed = 0.5f;
    [SerializeField]
    private float autoMoveSpeed = 2;
    public Vector3 targetPosition;

    private Rigidbody rb;

    private Vector3 moveAmmount;
    private Vector3 smoothMoveVelocity;

    private bool preMoveCheck;
    private bool autoMove;

    void OnEnable() {
        if (!PlayerMovement.pm) {
            PlayerMovement.pm = this;
        } else {
            Destroy(this);
        }

        player = transform.parent;
        cam = this.transform;
        forwardObject = player.GetChild(1);
        rb = player.GetComponent<Rigidbody>();
        isMoving = false;
        preMoveCheck = false;
    }



    // Update is called once per frame
    void Update() {

        if (autoMove) {
            MoveToPoint();
        } else {
            if (isMoving) {
                MoveForwards();
            } else {
                StopMovement();
            }
        }
    }

    void FixedUpdate() {
        rb.MovePosition(rb.position + moveAmmount * 
            Time.deltaTime);
    }

    void MoveForwards() {
        Vector3 moveDir = cam.forward;
        moveDir.y = 0;

        Vector3 targetMoveAmmount = moveDir * moveSpeed;
        moveAmmount = Vector3.SmoothDamp(moveAmmount,targetMoveAmmount,
            ref smoothMoveVelocity,.15f);
    }

    void MoveToPoint() {
        player.position = Vector3.Lerp(player.position,
            pm.targetPosition,
            Time.deltaTime * moveSpeed);
    }

    void StopMovement() {
        moveAmmount = Vector3.zero;
    }

    public void ButtonEnter() {
        preMoveCheck = true;
    }

    public void ButtonExit() {
        if (preMoveCheck) {
            isMoving = !isMoving;
        }

        preMoveCheck = false;
    }

    public void SetMovement(bool move) {
        isMoving = false;
        preMoveCheck = false;
        autoMove = move;
    } 
}
