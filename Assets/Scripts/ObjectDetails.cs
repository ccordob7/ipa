﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using System.Linq;

public class ObjectDetails : MonoBehaviour {

    [Header("Vocabulary")]
    public string translation;
    public VideoClip video;

    [System.Serializable]
    public class Interactions {
        public string sentenceTranslation;
        public GameObject[] interactableObjects;
        public VideoClip videoTranslation;
    }
    [Header("Sentences")]
    public Interactions[] interactions = new Interactions[0];

    [Header("Visuals")]
    public Material[] activatedMaterials;
    public Material[] currentMaterials;

    private bool isActivated;
    private bool activeCheck;

    public bool isRunning;

    private bool isLooking;
    private float lookTimer;
    public float totalLook = 2f;
    public float resetTimer = 10f;

    // Start is called before the first frame update
    void Start() {
        List<Material> curMats = new List<Material>();
        foreach(MeshRenderer mesh in transform.GetChild(0).GetComponentsInChildren<MeshRenderer>()) {
            for(int i = 0;i < mesh.materials.Length;i++) {
                curMats.Add(mesh.materials[i]);
            }
        }
        if(curMats.Count == 0) {
            foreach (SkinnedMeshRenderer mesh in transform.GetComponentsInChildren<SkinnedMeshRenderer>()) {
                for(int i = 0;i < mesh.materials.Length;i++) {
                    curMats.Add(mesh.materials[i]);
                }
            }
        }

        lookTimer = 0;
        currentMaterials = curMats.ToArray();

    }

    // Update is called once per frame
    void Update() {
        if(isActivated != activeCheck) {
            ChangeMaterials(isActivated);
            activeCheck = isActivated;
        }

        if (isLooking) {
            lookTimer += Time.deltaTime;
        }

        if(lookTimer > totalLook) {
            ActivateObjects(true);
            StopTimer();
        }
    }

    void ChangeMaterials(bool activated) {
        if (activated) {
            int mats = 0;
            MeshRenderer[] renderers = transform.GetChild(0).GetComponentsInChildren<MeshRenderer>();
            int childrenSize = renderers.Length;
            if (childrenSize > 0) {
                ChangeMeshRenderers(renderers,mats,activated);
            } else {
                SkinnedMeshRenderer[] renderersSk = transform.GetChild(0).GetComponentsInChildren<SkinnedMeshRenderer>();
                ChangeSkinnedMeshRenderer(renderersSk,mats,activated);
            }
            
        } else {
            int mats = 0;
            MeshRenderer[] renderers = transform.GetChild(0).GetComponentsInChildren<MeshRenderer>();
            int childrenSize = renderers.Length;
            if (childrenSize > 0) {
                ChangeMeshRenderers(renderers,mats,activated);
            } else {
                SkinnedMeshRenderer[] renderersSk = transform.GetChild(0).GetComponentsInChildren<SkinnedMeshRenderer>();
                ChangeSkinnedMeshRenderer(renderersSk,mats,activated);
            }
        }
    }

    void ChangeMeshRenderers(MeshRenderer[] renderers, int mats, bool isActivated) {
        if (isActivated) {
            foreach (MeshRenderer mesh in renderers) {
                Material[] tempArray = new Material[mesh.materials.Length];
                for (int i = 0;i < mesh.materials.Length;i++) {
                    tempArray[i] = activatedMaterials[mats];
                    mats++;
                }
                mesh.materials = tempArray;
            }
        } else {
            foreach (MeshRenderer mesh in renderers) {
                Material[] tempArray = new Material[mesh.materials.Length];
                for (int i = 0;i < mesh.materials.Length;i++) {
                    tempArray[i] = currentMaterials[mats];
                    mats++;
                }
                mesh.materials = tempArray;
            }
        }
    }

    void ChangeSkinnedMeshRenderer(SkinnedMeshRenderer[] renderersSk, int mats, bool isActivated) {
        if (isActivated) {
            foreach (SkinnedMeshRenderer mesh in renderersSk) {
                Material[] tempArray = new Material[mesh.materials.Length];
                for (int i = 0;i < mesh.materials.Length;i++) {
                    tempArray[i] = activatedMaterials[mats];
                    mats++;
                }
                mesh.materials = tempArray;
            }
        } else {
            foreach (SkinnedMeshRenderer mesh in renderersSk) {
                Material[] tempArray = new Material[mesh.materials.Length];
                for (int i = 0;i < mesh.materials.Length;i++) {
                    tempArray[i] = currentMaterials[mats];
                    mats++;
                }
                mesh.materials = tempArray;
            }
        }
    }

    void ActivateObjects(bool activation) {
        if (InteractionManager.im.vocabulary) {
            if (activation) {
                if (isRunning) {
                    StopCoroutine(RemoveText());
                }
                ItemTranslation.itemsTr.SetObjectText(translation);
                ItemTranslation.itemsTr.SetObjectVideo(video);
                Transform child = InteractionManager.im.currentSpot.transform.GetChild(0);
                for (int i = 0;i < child.childCount;i++) {
                    if (child.GetChild(i).GetComponent<ObjectDetails>()) {
                        if (!child.GetChild(i).GetComponent<ObjectDetails>().Equals(this)) {
                            child.GetChild(i).GetComponent<ObjectDetails>().enabled = false;
                            if (child.GetChild(i).GetComponent<Collider>()) {
                                child.GetChild(i).GetComponent<Collider>().enabled = false;
                            }
                        }
                    }
                }

                StartCoroutine("RemoveText");
            }
        } else {
            InteractionManager.im.CheckInteraction(this, activation);
        }
        
    }

    public void SetActivation(bool activation) {
        isActivated = activation;
    }

    public bool GetActivation() {
        return isActivated;
    }

    IEnumerator RemoveText() {
        isRunning = true;
        yield return new WaitForSeconds(resetTimer);
        ItemTranslation.itemsTr.ResetCanvas();
        Transform child = InteractionManager.im.currentSpot.transform.GetChild(0);
        for (int i = 0;i < child.childCount;i++) {
            if (child.GetChild(i).GetComponent<ObjectDetails>()) {
                if (!child.GetChild(i).GetComponent<ObjectDetails>().Equals(this)) {
                    child.GetChild(i).GetComponent<ObjectDetails>().enabled = true;
                    if (child.GetChild(i).GetComponent<Collider>()) {
                        child.GetChild(i).GetComponent<Collider>().enabled = true;
                    }
                }
            }
        }
        isRunning = false;
    }

    IEnumerator ResetMaterials() {
        yield return new WaitForSeconds(resetTimer);
        StopTimer();
        ActivateObjects(false);
        InteractionManager.im.currentInteraction = new List<ObjectDetails>();
    }

    public void StartTimer() {
        isLooking = true;
    }

    public void StopTimer() {
        isLooking = false;
        lookTimer = 0;
    }
}
