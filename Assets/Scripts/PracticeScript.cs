﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PracticeScript : MonoBehaviour {

    [SerializeField]
    public float introTime = 2.5f;

    [System.Serializable]
    class TranslationObject {
        public string[] objectSpanish;
        public Sprite objectImage;
    }
    [SerializeField]
    TranslationObject[] objectList;

    [SerializeField]
    private GameObject Board;
    [SerializeField]
    private Text translationField;
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Image[] imageField;

    private List<int> selectedImages = new List<int>();
    [SerializeField]
    private bool switchQuestion = false;
    private int previousWord = 0;
    private int currentScore = 0;
    private int totalAnswers = 0;
    private int correctAnswer;

    [Header("Effects")]
    [SerializeField]
    private GameObject rightSelectionEffect;
    [SerializeField]
    private GameObject wrongSelectionEffect;

    // Start is called before the first frame update
    void Start() {
        translationField.enabled = false;
        for(int i=0;i < imageField.Length;i++) {
            imageField[i].enabled = false;
        }
        scoreText.enabled = false;
        //Make the board grow.

        StartCoroutine("StartPractice");
        StartCoroutine(ScaleOverTime(introTime-0.5f));
    }

    // Update is called once per frame
    void Update() {
        if (switchQuestion) {
            switchQuestion = false;
            selectedImages = new List<int>();
            FillNextQuestion();
        }
    }

    IEnumerator StartPractice() {
        yield return new WaitForSeconds(introTime);
        FillNextQuestion();
    }

    IEnumerator ScaleOverTime(float time) {
        Vector3 originalScale = Board.transform.localScale;
        Vector3 destinationScale = new Vector3(0.05f,0.05f,0.05f);

        float currentTime = 0.0f;

        do {
            Board.transform.localScale = Vector3.Lerp(originalScale,destinationScale,currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);

        scoreText.enabled = true;
    }

    void FillNextQuestion() {
        correctAnswer = 0;
        int randSelection = 0;
        do {
            randSelection = Random.Range(0,objectList.Length);
            if (randSelection > objectList.Length - 1) {
                randSelection = objectList.Length - 1;

            }
        } while (randSelection == previousWord);
        

        bool chosen = false;
        for(int i = 0;i < imageField.Length;i++) {
            if(i != imageField.Length - 1) {
                if (!chosen) {
                    bool randomChoser = (Random.value > 0.5f);
                    if (randomChoser) {
                        imageField[i].sprite = objectList[randSelection].objectImage;
                        chosen = true;
                        correctAnswer = i;
                    } else {
                        ChooseRandomSprite(randSelection,i);
                    }
                } else {
                    ChooseRandomSprite(randSelection,i);
                }
            } else {
                if (!chosen) {
                    imageField[i].sprite = objectList[randSelection].objectImage;
                } else {
                    ChooseRandomSprite(randSelection,i);
                }
            }
            imageField[i].enabled = true;
        }
        translationField.text = objectList[randSelection]
            .objectSpanish[Random.Range(0,objectList[randSelection].objectSpanish.Length)];
        translationField.enabled = true;
        previousWord = randSelection;
    }

    void ChooseRandomSprite(int currentSprite, int loc) {
        int randomSprite = currentSprite;
        do {
            randomSprite = Random.Range(0,objectList.Length);
            bool isInList = selectedImages.Contains(randomSprite);
        } while (randomSprite == currentSprite || selectedImages.Contains(randomSprite));

        imageField[loc].sprite = objectList[randomSprite].objectImage;
        selectedImages.Add(randomSprite);
    }

    public void ChooseAnswer(int answerChosen) {
        if (answerChosen == correctAnswer) {
            currentScore++;
            InstantiateEffect(rightSelectionEffect);
        } else {
            InstantiateEffect(wrongSelectionEffect);
        }

        totalAnswers++;
        scoreText.text = "Puntaje: " + currentScore + "/" + totalAnswers;
        switchQuestion = true;
    }

    void InstantiateEffect(GameObject selectionEffect) {
        Vector3 position = Camera.main.transform.position + Camera.main.transform.forward * 1.5f;
        GameObject effect = Instantiate(selectionEffect,position,Quaternion.identity);
        Destroy(effect,1.1f);
    }

    public void BackToMainMenu() {
        SceneManager.LoadScene(0);
        InteractionManager.im.ChangeScene(0);
    }
}
