﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraRaycast : MonoBehaviour {

    private bool isLooking;
    private float lookTimer;
    private float cooldownTimer;
    private bool isInCooldown;

    [SerializeField]
    private float watchTime = 2f;
    [SerializeField]
    private float cooldownTime = 3f;

    [SerializeField]
    private LayerMask hitMask;

    public GameObject hitTarget;

    // Update is called once per frame
    void Update() {
        CountLook();
        CheckCooldownTimer();
    }

    void CountLook() {
        if (isLooking) {
            lookTimer += Time.deltaTime;
            if(lookTimer >= watchTime) {
                if(hitTarget.GetComponent<FocalPoint>() != null) {
                    if (!isInCooldown) {
                        if(hitTarget.GetComponent<FocalPoint>() != null) {
                            FocalPoint point = hitTarget.GetComponent<FocalPoint>();
                            point.StartMovement(true);

                            if (point.isTransition && point.nextPosition[0] != InteractionManager.im.pastSpot) {
                                StartCoroutine(MoveToNextPoint(point));
                            }
                        }else if (hitTarget.GetComponent<Button>() != null) {


                        }
                        
                        isInCooldown = true;

                        ResetCounter();
                    }
                }
            }
        }
    }

    IEnumerator MoveToNextPoint(FocalPoint pointToMove) {
        while (pointToMove.isTransition && InteractionManager.im.pastSpot != pointToMove.nextPosition[0]) {
            yield return new WaitForSeconds(3f);
            pointToMove = pointToMove.nextPosition[0];
            pointToMove.StartMovement(true);
        }

        if (pointToMove.nextPosition.Length>1) {
            if (InteractionManager.im.pastSpot == pointToMove.nextPosition[0]) {
                if(InteractionManager.im.pastSpot != pointToMove.nextPosition[1]) {
                    yield return new WaitForSeconds(3f);
                    pointToMove = pointToMove.nextPosition[1];
                    pointToMove.StartMovement(true);
                }
            }
        }
    }

    IEnumerator TimerCheck() {
        yield return new WaitForSeconds(2);
        PlayerMovement.pm.SetMovement(false);
    }

    public void ResetCounter() {
        isLooking = false;
        lookTimer = 0;
    }

    void CheckCooldownTimer() {
        if (isInCooldown) {
            cooldownTimer += Time.deltaTime;
            if (cooldownTimer >= cooldownTime) {
                isInCooldown = false;

                StartCoroutine(TimerCheck());
                ResetCooldown();
            }
        }
    }

    void ResetCooldown() {
        cooldownTimer = 0;
    }

    public void SetHitTarget(GameObject hit) {
        hitTarget = hit;
    }

    public void StartCounter() {
        isLooking = true;
    }
}
