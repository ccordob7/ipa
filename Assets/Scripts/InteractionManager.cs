﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class InteractionManager : MonoBehaviour {
    public static InteractionManager im;

    public enum Scenes { menus, learning, practice, evaluation};
    public Scenes currentScene;

    public GameObject currentSpot;
    public FocalPoint pastSpot;
    public bool vocabulary = true;

    public List<ObjectDetails> currentInteraction = new List<ObjectDetails>();
    public bool isNew = true;
    public int currentSentenceLength=0;

    public float resetTimer = 10;
    public List<int> possibleSolutions = new List<int>();

    void OnEnable() {
        if (InteractionManager.im == null) {
            InteractionManager.im = this;
        } else {
            Destroy(this.gameObject);
        }

        //vocabulary = true;
        isNew = false;

        currentInteraction = new List<ObjectDetails>();
        DontDestroyOnLoad(this);
    }

    public void ChangeScene(int scene) {
        switch (scene) {
            case 1 :
            currentScene = Scenes.learning;
            break;

            case 2 :
            currentScene = Scenes.learning;
            break;

            case 3 :
            currentScene = Scenes.practice;
            break;

            case 4:
            currentScene = Scenes.evaluation;
            break;

            default:
            currentScene = Scenes.menus;
            break;
        }
    }

    public void CheckInteraction(ObjectDetails obj, bool activation) {

        if (activation) {
            

            currentInteraction.Add(obj);

            if (currentInteraction.Count == 2) {
                CheckSecondInteraction();
            }

            if (currentInteraction.Count == 1) {
                foreach (ObjectDetails.Interactions inter in obj.interactions) {
                    if (inter.interactableObjects != null) {
                        if (inter.interactableObjects[0].GetComponent<ObjectDetails>().GetActivation() != activation) {
                            inter.interactableObjects[0].GetComponent<ObjectDetails>().SetActivation(activation);
                        }
                    }
                }
            } else {
                if(possibleSolutions.Count == 1) {
                    SetMaterialsForActivation(activation,0);
                } else {
                    for(int i = 0;i < possibleSolutions.Count;i++) {
                        SetMaterialsForActivation(activation,i);
                    }
                }
            }

            if(currentInteraction.Count == currentSentenceLength && currentInteraction.Count>1) {
                ShowTranslation(currentInteraction[0].interactions[possibleSolutions[0]]);
                removeObjectsFromList();
            }

            StartCoroutine(ResetInteractions(resetTimer));
        } else {
            DisableAllEmissiveMaterials(obj, activation);
        }

        
    }

    void SetMaterialsForActivation(bool activation, int index) {
        for (int i = 1;i < currentInteraction[0].interactions[possibleSolutions[index]].interactableObjects.Length;i++) {
            if (currentInteraction[0].interactions[possibleSolutions[index]].interactableObjects[i]
                .GetComponent<ObjectDetails>().GetActivation() != activation) {
                currentInteraction[0].interactions[possibleSolutions[index]].interactableObjects[i]
                .GetComponent<ObjectDetails>().SetActivation(activation);
            }
        }
    }

    void DisableAllEmissiveMaterials(ObjectDetails obj, bool activation) {
        foreach (ObjectDetails.Interactions inter in obj.interactions) {
            if (inter.interactableObjects != null) {
                for (int i = 0;i < inter.interactableObjects.Length;i++) {
                    if (inter.interactableObjects[i].GetComponent<ObjectDetails>().GetActivation() != activation) {
                        inter.interactableObjects[i].GetComponent<ObjectDetails>().SetActivation(activation);
                    }
                }
            }
        }
    }

    void CheckSecondInteraction() {
        int index = 0;
        while (index < currentInteraction[0].interactions.Length) {
            if (currentInteraction[1].Equals(
                currentInteraction[0].interactions[index].interactableObjects[0].GetComponent<ObjectDetails>())) {
                possibleSolutions.Add(index);
            }
            index++;
        }
        if (possibleSolutions.Count == 1) {
            currentSentenceLength = currentInteraction[0].interactions[possibleSolutions[0]].interactableObjects.Length+1;
        }
    }

    void ShowTranslation(ObjectDetails.Interactions obj) {
        ItemTranslation.itemsTr.SetObjectText(obj.sentenceTranslation);
        ItemTranslation.itemsTr.SetObjectVideo(obj.videoTranslation);
        Transform child = currentSpot.transform.GetChild(0);
        for (int i = 0;i < child.childCount;i++) {
            if (child.GetChild(i).GetComponent<ObjectDetails>()) {
                if (!child.GetChild(i).GetComponent<ObjectDetails>().Equals(this)) {
                    child.GetChild(i).GetComponent<ObjectDetails>().enabled = false;
                    if (child.GetChild(i).GetComponent<Collider>()) {
                        child.GetChild(i).GetComponent<Collider>().enabled = false;
                    }
                }
            }
        }

        StartCoroutine(RemoveText());
    }

    bool FindIsNew(ObjectDetails obj) {
        bool result = true;
        possibleSolutions = new List<int>();
        for (int i = 0;i < currentInteraction[0].interactions.Length;i++) {
            for (int j = 0;j < currentInteraction[0].interactions[i].interactableObjects.Length;j++) {
                if (currentInteraction[0].interactions[i].interactableObjects[j].GetComponent<ObjectDetails>().Equals(obj)) {
                    if (!possibleSolutions.Contains(i)) {
                        possibleSolutions.Add(i);
                    }
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    IEnumerator ResetInteractions(float timer) {
        yield return new WaitForSeconds(timer);
        removeObjectsFromList();
        currentSentenceLength = 0;
    }

    IEnumerator RemoveText() {
        yield return new WaitForSeconds(resetTimer);
        ItemTranslation.itemsTr.ResetCanvas();
        Transform child = currentSpot.transform.GetChild(0);
        for (int i = 0;i < child.childCount;i++) {
            if (child.GetChild(i).GetComponent<ObjectDetails>()) {
                if (!child.GetChild(i).GetComponent<ObjectDetails>().Equals(this)) {
                    child.GetChild(i).GetComponent<ObjectDetails>().enabled = true;
                    if (child.GetChild(i).GetComponent<Collider>()) {
                        child.GetChild(i).GetComponent<Collider>().enabled = true;
                    }
                }
            }
        }
    }

    void removeObjectsFromList() {
        foreach (ObjectDetails obj in currentInteraction) {
            CheckInteraction(obj,false);
        }
        currentInteraction = new List<ObjectDetails>();
        possibleSolutions = new List<int>();
    }
}
