﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTransparency : MonoBehaviour {

    [SerializeField]
    private float fadeSpeed = 1;

    private GameObject transparencyCube;
    private MeshRenderer mRenderer;

    public bool isFading;
    public bool faded;

    // Start is called before the first frame update
    void OnEnable() {
        transparencyCube = this.gameObject;
        mRenderer = transparencyCube.GetComponent<MeshRenderer>();
    }

    IEnumerator FadeOut() {
        for(float t = fadeSpeed;t>= -0.5f;t -= 0.05f) {
            Color c = mRenderer.material.color;
            c.a = t;
            mRenderer.material.color = c;
            yield return new WaitForSeconds(0.05f);
        }

        faded = true;
    }

    public void ButtonPressedEnter() {
        isFading = true;
        //cube.SetActive(false);
        StartCoroutine("FadeOut");
        StartCoroutine("DestroyObject");
    }

    public void ButtonPressedExit() {
        Application.Quit();
    }

    IEnumerator DestroyObject() {
        yield return new WaitForSeconds(2);
        Destroy(this.gameObject);
    }
}
