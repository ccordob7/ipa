﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuAccess : MonoBehaviour {
    public static MenuAccess ma;

    [Header("Menus")]
    [SerializeField]
    private GameObject approachSelection;
    [SerializeField]
    private GameObject learningAmbientSelection;

    [Header("Objects")]
    [SerializeField]
    private GameObject transparencyCube;
    [SerializeField]
    private Toggle vocabularyToggle;

    void Start() {
        if(MenuAccess.ma == null) {
            MenuAccess.ma = this;
        } else {
            Destroy(this);
        }

        if(vocabularyToggle!= null) {
            vocabularyToggle.isOn = !InteractionManager.im.vocabulary;
        }
        
    }

    

    // Update is called once per frame
    void Update() {
        
    }

    public void AccessLearning() {
        approachSelection.SetActive(false);
        transparencyCube.SetActive(true);
        learningAmbientSelection.SetActive(true);
        transparencyCube.GetComponent<ChangeTransparency>().ButtonPressedEnter();
    }

    public void LoadScene(int sceneToLoad) {
        SceneManager.LoadScene(sceneToLoad);
        InteractionManager.im.ChangeScene(sceneToLoad);
    }

    public void ChangeVoc() {
        vocabularyToggle.isOn = !vocabularyToggle.isOn;

        InteractionManager.im.vocabulary = !vocabularyToggle.isOn;
    }
}
