﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraForwardCalculator : MonoBehaviour {

    private Transform cam;
    public float xRotation;

    void OnEnable() {
        cam = transform.parent.GetChild(0);
    }

    // Update is called once per frame
    void Update() {
        Vector3 rotation = cam.transform.localRotation.eulerAngles;
        float rotationY = rotation.y;
        rotation.x = xRotation;
        rotation.z = 0;

        transform.rotation = Quaternion.Euler(rotation);
    }
}
