﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocalPoint : MonoBehaviour {

    public Transform thisTransform;
    public bool isTransition = false;
    public bool isEnding = false;
    public FocalPoint[] nextPosition;

    [Header("Movement")]
    public bool moveTowards;

    private bool isMoving;

    // Update is called once per frame
    void Update() {
        if (moveTowards) {
            MoveCamera();
        }
    }

    void MoveCamera() {
        Vector3 posToMove = this.transform.position;
        posToMove.y += PlayerMovement.pm.cameraHeight;
        PlayerMovement.pm.targetPosition = posToMove;
        PlayerMovement.pm.SetMovement(true);

        moveTowards = false;
    }

    public void StartMovement(bool move) {
        moveTowards = move;

        if (!isMoving) {
            isMoving = true;
            DisablePastFocalPoint();
            EnableCurrentFocalPoint();
        }
        
    }

    void DisablePastFocalPoint() {
        if (InteractionManager.im.currentSpot != null) {
            if (InteractionManager.im.currentSpot.transform.childCount > 0) {
                Transform child = InteractionManager.im.currentSpot.transform.GetChild(0);
                for (int i = 0;i < child.childCount;i++) {
                    if (child.GetChild(i).GetComponent<ObjectDetails>()) {
                        child.GetChild(i).GetComponent<ObjectDetails>().enabled = false;
                    }
                    if (child.GetChild(i).GetComponent<Collider>()) {
                        child.GetChild(i).GetComponent<Collider>().enabled = false;
                    }
                }
            }

            InteractionManager.im.pastSpot = InteractionManager.im.currentSpot.GetComponent<FocalPoint>();
        }

        InteractionManager.im.currentSpot = gameObject;
    }

    void EnableCurrentFocalPoint() {
        if (transform.childCount>0) {
            Transform child = transform.GetChild(0);
            for (int i = 0;i < child.childCount;i++) {
                if (child.GetChild(i).GetComponent<ObjectDetails>()) {
                    child.GetChild(i).GetComponent<ObjectDetails>().enabled = true;
                }
                if (child.GetChild(i).GetComponent<Collider>()) {
                    child.GetChild(i).GetComponent<Collider>().enabled = true;
                }
            }
        }

        isMoving = false;
    }
}
